# appdirect-code-challenge-solution

## Setup

Install the project dependencies:

`npm install`

Rename the file `config-sample.json` to `config.json` and paste Twitter API key and secret into the appropriate fields. To get them you can try directions in [this StackOverflow question](http://stackoverflow.com/questions/1808855/getting-new-twitter-api-consumer-and-secret-keys).

## Testing

The following fires up a PhantomJS browser, runs tests and exists:

`npm test`

If you want the server to stay running during development (it will re-run tests on each code change), just change `singleRun` to `false` in `karma.conf.js`.

## Running

Starts the static and proxy servers:

`npm start`

## Features

* Tweets should have well-formatted date, link to tweet itself (as a link on the date), tweet body (as text)
* Edit layout as a full-screen modal dialog
* Number of tweets controlled via a slider feature in settings
* Twitter handles to fetch are controlled via a 'chips'-type text input in settings
* Order of the columns controlled via a drag-and-drop feature in settings
* LocalStorage to persist settings ("Save settings" button needs to be clicked - canceling or closing the settings modal is not sufficient).
* Fully responsive UI (comes for free with Angular Material)
* Loading indicator in top left corner

## Missing features (present in doc but not implemented)

* "For retweets and mentions, the username should be included" - unclear instruction, e-mailed Jared about it and was asked to wait
* Time range of the tweets shown
* Overall palette/skin of the page. Not easily done due to the nature of Angular Material theme engine, which is loaded in `app.config()`.

## Wishlist

* Tests!
* Better error-handling on twitter handle text input in settings: right now if there are three default twitter handles specified and user tries to type a fourth one then hit 'Enter' - nothing happens and there is no error message.