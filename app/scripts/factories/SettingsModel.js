( function() {
    'use strict';

    /**
     * @ngdoc function
     * @name appdirectApp.factory:SettingsModel
     * @description
     * # SettingsModel
     * Settings model for passing various settings around
     */
    angular.module( 'appdirectApp' )
        .factory( 'SettingsModel', [ '$log', SettingsModel ] );

    function SettingsModel( $log ) {

        SettingsModel.defaults = {
            count: 30,
            maxChannels: 3,
            channels: [ 'AppDirect', 'laughingsquid', 'techcrunch' ]
        };

        /**
         * Constructor, with class name
         */
        function SettingsModel( count, maxChannels, channels ) {

            // Public properties, assigned to the instance ('this')
            this.count = count || SettingsModel.defaults.count;
            this.maxChannels = maxChannels || SettingsModel.defaults.maxChannels;
            this.channels = channels || SettingsModel.defaults.channels;

        }

        /**
         * Static method, assigned to class
         * Instance ('this') is not available in static context
         */
        SettingsModel.build = function( data ) {

            return new SettingsModel(
                data.count || null,
                data.maxChannels || SettingsModel.defaults.maxChannels,
                data.channels || SettingsModel.defaults.channels
            );
        };

        /**
         * Return the constructor function
         */
        return SettingsModel;
    }

} )();