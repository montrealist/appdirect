( function() {
    'use strict';

    /**
     * @ngdoc service
     * @name appdirectApp.service:TwitterService
     * @description
     * # TwitterService
     * Service for fetching tweets from Twitter API
     */
    function TwitterService( $log, $q, $http ) {
        var self = this;
        var baseUrl = 'http://localhost:7890/1.1/statuses/user_timeline.json?';

        function wrapTweetMention( text, substringToWrap ) {
            return text.replace( substringToWrap, '<em class="mention">$&</em>' );
        }

        function wrapTweetLink( text, substringToWrap ) {
            return text.replace( substringToWrap, '<a href="$&">$&</a>' );
        }

        self.normalize = function( data ) {
            var output = [];
            angular.forEach( data, function( item, key ) {
                var obj = {};

                obj.created_at = new Date(
                    item.created_at.replace( /^\w+ (\w+) (\d+) ([\d:]+) \+0000 (\d+)$/,
                        "$1 $2 $4 $3 UTC" ) ) || null;

                var tweet;

                /* process retweet author */
                if ( item.retweeted_status ) {
                    tweet = item.retweeted_status;
                    obj.retweet = {};
                    obj.retweet.name = tweet.user.name || null;
                    obj.retweet.screen_name = tweet.user.screen_name || null;

                } else {
                    tweet = item;
                }
                obj.text = tweet.text || null;

                /* process link inside tweet */
                if ( tweet.entities.media && tweet.entities.media.length && tweet.entities.media[ 0 ].url ) {
                    obj.text = wrapTweetLink( tweet.text, tweet.entities.media[ 0 ].url );

                } else if ( tweet.entities.urls && tweet.entities.urls.length ) {
                    obj.text = wrapTweetLink( tweet.text, tweet.entities.urls[ 0 ].url );
                }

                /* process mentions */
                if ( tweet.entities && tweet.entities.user_mentions && tweet.entities.user_mentions.length ) {

                    angular.forEach( tweet.entities.user_mentions, function( mention, key ) {
                        obj.text = wrapTweetMention( obj.text, mention.screen_name );
                    } );
                }

                obj.url = ( item.id_str ) ? '//twitter.com/' + item.user.screen_name + '/status/' + item.id_str : '//twitter.com/';
                // obj.url = ( ( item.entities && item.entities.media ) ? item.entities.media[ 0 ].url : 'http://twitter.com/' ) || null;
                this.push( obj );
            }, output );
            return output;
        };

        self.getWithParams = function( parameters ) {
            var params = { count: parameters.count || 20, screen_name: parameters.screen_name || '' };

            return $http( {
                    method: 'GET',
                    url: baseUrl,
                    params: params
                } )
                .then( function( response ) {
                    var obj = {};
                    obj.screen_name = params.screen_name;
                    obj.data = self.normalize( response.data ); /* get only what we need from each tweet's JSON */
                    return obj;

                }, function( err ) {
                    return err;
                } );
        };
    }

    angular.module( 'appdirectApp' )
        .service( 'TwitterService', [ '$log', '$q', '$http', TwitterService ] );

} )();