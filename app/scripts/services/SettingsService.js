( function() {
  'use strict';

  /**
   * @ngdoc function
   * @name appdirectApp.service:SettingsService
   * @description
   * # SettingsService
   * Service for fetching/persisting page settings
   */
  angular.module( 'appdirectApp' )
    .service( 'SettingsService', [ '$log', '$rootScope', 'SettingsModel', SettingsService ] );

  function SettingsService( $log, $rootScope, SettingsModel ) {

    var self = this;

    self.syncSettings = function() {
      sessionStorage.settings = angular.toJson( self.settings );
    };

    self.updateSettings = function( obj ) {
      if ( obj ) {
        self.settings = SettingsModel.build( obj );
        self.syncSettings();
        return self.settings;
      } else {
        return false;
      }
    };

    self.loadSettings = function() {
      var settings = angular.fromJson( sessionStorage.settings );
      if ( settings ) {
        self.updateSettings( settings );
        return self.settings;
      } else {
        return new SettingsModel();
      }
    };

    self.settings = self.loadSettings();
  }

} )();