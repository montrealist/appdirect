( function() {
  'use strict';

  /**
   * @ngdoc function
   * @name appdirectApp.controller:MainCtrl
   * @description
   * # MainCtrl
   * Main controller for the page
   */
  angular.module( 'appdirectApp' )
    .controller( 'MainCtrl', [ '$log', '$scope', '$rootScope', '$q', '$mdDialog', '$mdMedia', 'SettingsService', 'TwitterService', MainCtrl ] );

  /* inserting a helper controller right at the spot - it's getting late. */
  function DialogController( $log, $scope, $rootScope, $mdDialog, $mdMedia, SettingsService ) {

    $scope.settings = SettingsService.loadSettings();

    $scope.save = function() {
      $scope.updateSettings();
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.updateSettings = function() {
      $rootScope.$broadcast( 'settings:updated', $scope.settings );
      $scope.settings = SettingsService.updateSettings( $scope.settings );
    };
  }

  function MainCtrl( $log, $scope, $rootScope, $q, $mdDialog, $mdMedia, SettingsService, TwitterService ) {
    /* jshint validthis: true */
    var vm = this;

    vm.loading = { done: undefined, progress: 'indeterminate' }; /* two states of the 'loading' indicator */
    vm.waiting = vm.loading.done; /* variable controlling the 'loading' indicator */
    vm.output = [];
    vm.settings = {};

    $scope.$on( 'settings:updated', function( event, settings ) {
      vm.init( settings );
    } );

    vm.showSettings = function( ev ) {

      $mdDialog.show( {
        controller: DialogController,
        templateUrl: 'app/views/dialog.html',
        parent: angular.element( document.body ),
        targetEvent: ev,
        clickOutsideToClose: true,
        fullscreen: true
      } );

    };

    // vm.updateSettings = function() {
    //   vm.settings = SettingsService.updateSettings( vm.settings );
    //   vm.init();
    // };

    vm.getAllChannels = function( channelArray ) {
      var promises = [];
      var channels = channelArray || vm.settings.channels;

      angular.forEach( channels, function( channel ) {

        var promise = TwitterService.getWithParams( { count: vm.settings.count, screen_name: channel } );

        promises.push( promise );

      } );

      return $q.all( promises );
    };

    /* get an error object and parse it into an object displayable on page */
    function createError( obj ) {
      var errorObj = {},
        errorStatuses = [ -1, 400, 401 ];

      if ( inArray( obj.status, errorStatuses ) ) {

        errorObj.statusText = 'Did you add the API keys in /config.json?';
        errorObj.status = obj.status;
      } else {

        if ( obj.status ) {
          errorObj.status = obj.status;
        }
        if ( obj.statusText ) {
          errorObj.statusText = obj.statusText;
        }
      }
      if ( obj.config && obj.config.params && obj.config.params.screen_name ) {
        errorObj.screen_name = obj.config.params.screen_name;
      }
      return errorObj;
    }

    function inArray( value, array ) {
      return array.indexOf( value ) > -1;
    }

    vm.init = function( settings ) {
      vm.settings = settings || SettingsService.loadSettings();

      vm.waiting = vm.loading.progress;

      vm.getAllChannels()
        .then( function( payload ) {
          vm.waiting = vm.loading.done;
          /* all promises done; let's prepare payload for displaying on page */
          var output = [];
          angular.forEach( payload, function( value, key ) {
            var obj = {};

            /* if got an object - then it's an error */
            if ( inArray( Object.prototype.toString.call( value.data ), [ '[object Object]', '[object Null]' ] ) ) {
              obj.error = createError( value );
            } else {
              obj.screen_name = value.screen_name;
              obj.tweets = value.data;
            }
            this.push( obj );
          }, output );

          vm.output = output;

        } );

    };

    vm.init(); // all aboard!

  }
} )();