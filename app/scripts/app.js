( function() {
  'use strict';

  /**
   * @ngdoc overview
   * @name oktpusApp
   * @description
   * # oktpusApp
   *
   * Main module of the application.
   */
  angular
    .module( 'appdirectApp', [
      'ngAnimate',
      'ngAria',
      'ngMessages',
      'ngRoute',
      'ngSanitize',
      'ngMaterial',
      'ui.router',
      'angular-sortable-view'
    ] )
    .config( function( $logProvider, $stateProvider, $urlRouterProvider, $mdThemingProvider ) {

      $logProvider.debugEnabled( true );

      $mdThemingProvider.theme( 'default' )
        .primaryPalette( 'blue-grey' )
        .accentPalette( 'lime' );

      $urlRouterProvider
        .otherwise( '/' );

      $stateProvider
        .state( 'main', {
          url: '/',
          templateUrl: 'app/views/main.html',
          controller: 'MainCtrl as main'
        } );

    } );
} )();