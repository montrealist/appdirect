describe( 'main controller', function() {

    beforeEach( module( 'appdirectApp' ) );

    var $controller;

    beforeEach( inject( function( _$controller_, _$rootScope_ ) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
    } ) );

    it( "getAllChannels should be defined", function() {
        var scope = $rootScope.$new();
        var log = {};
        var controller = $controller( 'MainCtrl', { $log: log, $scope: scope } );
        expect( controller.getAllChannels )
            .toBeDefined();
    } );

} );