describe( 'SettingsService', function() {

    beforeEach( module( 'appdirectApp' ) );

    var $controller;

    beforeEach( inject( function( _SettingsService_, _SettingsModel_ ) {
        SettingsService = _SettingsService_;
        SettingsModel = _SettingsModel_;
    } ) );

    it( 'call to loadSettings() should always return an object of type SettingsModel', function() {

        var settings = SettingsService.loadSettings();

        expect( settings )
            .toEqual( jasmine.any( SettingsModel ) );
    } );

    it( 'call to updateSettings should update the tweet count setting', function() {

        var newCount = 2;
        var newSettings = { count: newCount };

        var settings = SettingsService.updateSettings( newSettings );

        expect( SettingsService.settings.count )
            .toEqual( newCount );
    } );

} );